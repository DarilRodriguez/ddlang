package main

import (
  "fmt"
)

const (
  CALL    = 1
  DO      = 2
  IDENT   = 3
  VALUE   = 4

  ENDSUB  = 6
)

type NodeList []Node

type Node struct {
  Func int32
  Name string
  Tok int32
  Lit string
  Sub NodeList
}

func (n *NodeList) add(s Node) {
  _tmp := make([]Node, len(*n) + 1)
  copy(_tmp, *n)
  _tmp[len(*n)] = s
  *n = make([]Node, len(_tmp))
  copy(*n, _tmp)
}

func (n Node) print(t int32) {
  fmt.Printf("\n")
  for i := t; i > 0; i-- {
    fmt.Printf("\t")
  }
  fmt.Printf("Func: %v\n", n.Func)

  for i := t; i > 0; i-- {
    fmt.Printf("\t")
  }
  fmt.Printf("Name: %s\n", n.Name)

  for i := t; i > 0; i-- {
    fmt.Printf("\t")
  }
  fmt.Printf("Sub:\n")

  for _, s := range n.Sub {
    s.print(t + 1)
  }
}
