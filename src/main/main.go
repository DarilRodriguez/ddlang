package main

import (
  //"fmt"
  "log"
  "os"
  "go/scanner"
  "go/token"
)

func ReadFile(path string) ([]byte) {
  //read file info
  inf, err := os.Stat(path)
  if err != nil {
    log.Fatal(err)
  }
  buf := make([]byte, inf.Size())

  //read file content
  file, err := os.Open(path)
  if err != nil {
    log.Fatal(err)
  }

  _, err = file.Read(buf)
  if err != nil {
    log.Fatal(err)
  }

  return buf
}

func GetScanner(str []byte) (s scanner.Scanner) {
  fset := token.NewFileSet()
  file := fset.AddFile("", fset.Base(), len(str))
  s.Init(file, str, nil, scanner.ScanComments)
  return s
}

func Parse(s *scanner.Scanner, sub bool) (node Node){
  for {
    _, tok, _ := s.Scan()

    switch tok {
    case token.REM:
      _, tok, lit := s.Scan()

      switch tok {
      case token.IDENT:

        switch {
        case lit == "do":
          node.Func = DO

          _, tok, lit = s.Scan()
          if tok == token.IDENT {
            node.Name = lit
            _, tok, _ = s.Scan()
          }

          if tok == token.LBRACE {
            _node := Parse(s, true);
            for _node.Func != ENDSUB {
              node.Sub.add(_node)
              _node = Parse(s, true);
            }
          }

          return node
        }

      case token.LBRACE:
        node.Func = CALL


        return node

      }
    }

    if tok == token.EOF {
      break
    } else if sub && tok == token.RBRACE {
      node.Func = ENDSUB
      break
    }

  }
  return node
}

func main() {
  s := GetScanner(ReadFile("main.ddl"))
  node := Parse(&s, false)
  node.print(0)
}
